# Create your views here.
import json

from django.http import JsonResponse, HttpResponse
from django.views import generic

from .models import Item


class IndexView(generic.ListView):
    """ E' possibile in alternativa a definire questi attributi fare override dei
     metodi corrispondenti
     es: queryset corrisponde al metodo get_queryset() """
    model = Item
    template_name = 'index.html'
    context_object_name = 'all_objects_list'
    queryset = Item.objects.filter(is_valid=True)


# Risponde alla chiamata post axios facendo una query sul DB e
# inviando la risposta al client in formato JSON
def posted(request, id):
    # data = request.body.decode()
    # data2 = [block for block in data.split('{')]
    obj = Item.objects.get(pk=id)
    #response = {
    #    'title': obj.title,
    #    'text': obj.text
    #}
    # id = data['id']
    return HttpResponse(obj.title, content_type='text/plain')


