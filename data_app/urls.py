from django.urls import path
from . import views

app_name = 'data_app'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:id>/posted/', views.posted, name='posted')
]