import datetime

from django.db import models


# Create your models here.
from django.utils import timezone


class Item(models.Model):
    title = models.CharField(max_length=50)
    text = models.CharField(max_length=200)
    pub_date = models.DateTimeField(default=timezone.now())
    is_valid = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    def has_one_word_title(self):
        return self.title.split(' ').__len__() == 1

    def is_published_recently(self):
        return self.pub_date >= timezone.now()-datetime.timedelta(days=1)