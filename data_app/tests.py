import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from .models import Item


# Create your tests here.

def create_item(title, text, days_offset):
    time = timezone.now() + datetime.timedelta(days=days_offset)
    return Item.objects.create(title=title, text=text, pub_date=time)


class ItemTests(TestCase):
    def test_item_title_has_more_than_one_word_separated_by_commas(self):
        """ test has_one_word_title ritorna  false per frasi formate da parole sseparate da
        caratteri di ìversi dallo spazio """
        multi_word_title_object = Item(title='Più,di,una,parola', text='Descrizione')
        self.assertIs(multi_word_title_object.has_one_word_title(), False)

    def test_item_title_has_more_than_one_word_separated_by_parentesis(self):
        """ test has_one_word_title ritorna  false per frasi formate da parole sseparate da
        caratteri di ìversi dallo spazio """
        multi_word_title_object = Item(title='Parola(fra_parentesi)', text='Descrizione')
        self.assertIs(multi_word_title_object.has_one_word_title(), False)

    def test_no_objects(self):
        """ Se non ci sono oggetti da mostrare viene ritornato un messaggio """
        response = self.client.get(reverse('data_app:index'))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'No objects available')

    def test_is_published_recently_with_pub_date_in_the_future(self):
        """ is_published recently ritorna false per oggetti creati nel futuro """
        future_item = create_item(title='Teletrasporto', text='Item of the future',
                                  days_offset=3000)
        self.assertIs(future_item.is_published_recently(), False)

    def test_is_published_recently_with_pub_date_in_the_far_past(self):
        """ is_published_recently ritorna False per oggetti creati più di una settimana fa """
        far_past_item = create_item(title='Ruota', text='Oggetto rotondo',
                                    days_offset=-2000)
        self.assertIs(far_past_item.is_published_recently(), False)

    def test_index_view_shows_only_valid_items(self):
        """ queryset contiene soltanto oggetti validi """
        create_item(title='Invalid', text='Invalid item', days_offset=0)
        response = self.client.get(reverse('data_app:index'))
        self.assertEquals(response.status_code, 200)
        self.assertNotContains(response.context['all_objects_list'], ['<Item: Invalid>'])